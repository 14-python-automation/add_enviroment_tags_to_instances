import boto3

ec2_client_toronto = boto3.client('ec2', region_name="ca-central-1")
ec2_resource_toronto = boto3.resource('ec2', region_name="ca-central-1")

ec2_client_ohio = boto3.client('ec2', region_name="us-east-2")
ec2_resource_ohio = boto3.resource('ec2', region_name="us-east-2")

instance_ids_toronto = []
instance_ids_ohio = []

reservations_toronto = ec2_client_toronto.describe_instances()['Reservations']
for res in reservations_toronto:
    instances = res['Instances']
    for ins in instances:
        instance_ids_toronto.append(ins['InstanceId'])


response = ec2_resource_toronto.create_tags(
    Resources=instance_ids_toronto,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

reservations_ohio = ec2_client_ohio.describe_instances()['Reservations']
for res in reservations_ohio:
    instances = res['Instances']
    for ins in instances:
        instance_ids_ohio.append(ins['InstanceId'])


response = ec2_resource_ohio.create_tags(
    Resources=instance_ids_ohio,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)